package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private EditText editCodigo;
    private EditText editNombre;
    private EditText editMarca;
    private EditText editPrecio;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Inicializar vistas
        editCodigo = findViewById(R.id.editCodigo);
        editNombre = findViewById(R.id.editNombre);
        editMarca = findViewById(R.id.editMarca);
        editPrecio = findViewById(R.id.editPrecio);

        // Configurar los botones
        Button btnGuardar = findViewById(R.id.btnGuardar);
        Button btnLimpiar = findViewById(R.id.btnLimpiar);
        Button btnNuevo = findViewById(R.id.btnNuevo);
        Button btnEditar = findViewById(R.id.btnEditar);

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                guardarDatos();
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiarCampos();
            }
        });

        btnNuevo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nuevoProducto();
            }
        });

        btnEditar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editarProducto();
            }
        });
    }

    private void guardarDatos() {
        String codigo = editCodigo.getText().toString();
        String nombre = editNombre.getText().toString();
        String marca = editMarca.getText().toString();
        String precioStr = editPrecio.getText().toString();

        if (codigo.isEmpty() || nombre.isEmpty() || marca.isEmpty() || precioStr.isEmpty()) {
            Toast.makeText(this, "Por favor, complete todos los campos", Toast.LENGTH_SHORT).show();
            return;
        }

        double precio = Double.parseDouble(precioStr);

        // Aquí puedes implementar la lógica para guardar los datos en una base de datos o hacer lo que necesites con ellos
        Toast.makeText(this, "Datos guardados:\nCódigo: " + codigo + "\nNombre: " + nombre + "\nMarca: " + marca + "\nPrecio: " + precio, Toast.LENGTH_SHORT).show();
    }

    private void limpiarCampos() {
        editCodigo.setText("");
        editNombre.setText("");
        editMarca.setText("");
        editPrecio.setText("");
    }

    private void nuevoProducto() {
        limpiarCampos();
    }

    private void editarProducto() {
        String codigo = editCodigo.getText().toString();
        String nombre = editNombre.getText().toString();
        String marca = editMarca.getText().toString();
        String precioStr = editPrecio.getText().toString();

        if (codigo.isEmpty() || nombre.isEmpty() || marca.isEmpty() || precioStr.isEmpty()) {
            Toast.makeText(this, "Por favor, complete todos los campos", Toast.LENGTH_SHORT).show();
            return;
        }

        double precio = Double.parseDouble(precioStr);

        // Aquí puedes implementar la lógica para editar el producto en la base de datos o hacer lo que necesites con él
        // Por simplicidad, aquí simplemente mostramos un mensaje con los datos del producto a editar
        Toast.makeText(this, "Editar producto:\nCódigo: " + codigo + "\nNombre: " + nombre + "\nMarca: " + marca + "\nPrecio: " + precio, Toast.LENGTH_SHORT).show();
    }
}